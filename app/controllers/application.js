import Controller from '@ember/controller';
import { action } from '@ember/object';
import { inject as service } from '@ember/service';
import { tracked } from '@glimmer/tracking';

export default class ApplicationController extends Controller {
  @service store
  @tracked bookName;
  queryBookName = '';

  @action addBook() {
    let newBook = this.store.createRecord('book', { name: this.bookName });
    newBook.save();
  }
}

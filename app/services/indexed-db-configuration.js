import IndexedDbConfigurationService from 'ember-indexeddb/services/indexed-db-configuration';

export default class ExtendedIndexedDbConfigurationService extends IndexedDbConfigurationService {
  currentVersion = 1;

  version1 = {
    stores: {
      'book': '&id,name'
    },
  };

  mapTable = {
    'book': (item) => {
      return {
        id: this._toString(item.id),
        json: this._cleanObject(item),
        name: item.name
      };
    }
  }
}

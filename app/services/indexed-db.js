import IndexedDbService from 'ember-indexeddb/services/indexed-db';

export default class ExtendedIndexedDbService extends IndexedDbService {
  _shouldLogQuery = true
}

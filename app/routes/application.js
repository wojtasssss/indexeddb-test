import Route from '@ember/routing/route';
import { inject as service } from '@ember/service';
import RSVP from 'rsvp';

export default class ApplicationRoute extends Route {
  @service indexedDb;

  beforeModel() {
    return this.indexedDb.setupTask.perform();
  }

  model() {
    return RSVP.hash({
      books: this.store.findAll('book'),
      queryBook: this.store.query('book', { name: 'Atomic Habbits' })
    });
  }
}
